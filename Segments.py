from LineSegment import LineSegment
import re
class Segments:
    def __init__(self):
        self.segmentList: list[LineSegment] = []
        self.collisionsList: list[tuple(LineSegment, LineSegment)] = []

    def BuildFromFile(self, path):
        with open(path) as p:
            # Read the first line
            line = p.readline()
            p1 = p2 = None
            while line:
               points = line.split(";")
               p1 = eval(re.sub("[^-\d ,.]","", points[0].strip(' ();')))
               if(p2): self.segmentList.append(LineSegment(p2, p1))
               else: firstPoint = p1
               p2 = eval(re.sub("[^-\d ,.]","", points[1].strip(' ();')))
               self.segmentList.append(LineSegment(p1,p2))
               # Iterate to the next line
               line = p.readline()
            # store the last segment
            self.segmentList.append(LineSegment(p2, firstPoint))

    def boundingBoxTest(self):
        self.collisionsList = []
        # check for collisions
        for i in range(len(self.segmentList)):
            for j in range(i):
                boundingBox = self.segmentList[i]
                for vertex in self.segmentList[j].boundingBoxXYZ:
                    if((vertex.x >= boundingBox.lowerXYZBound.x and vertex.x <= boundingBox.upperXYZBound.x) and
                       (vertex.y >= boundingBox.lowerXYZBound.y and vertex.y <= boundingBox.upperXYZBound.y) and
                       (vertex.z >= boundingBox.lowerXYZBound.z and vertex.z <= boundingBox.upperXYZBound.z)):
                        self.collisionsList.append((self.segmentList[i], self.segmentList[j]))
                        break
    def projectionTest(self):
        self.collisionsList = []
        for i in range(len(self.segmentList)):
            for j in range(i):
                boundingBox = self.segmentList[i]
                for vertex in self.segmentList[j].boundingBoxXY:
                    if((vertex.x >= boundingBox.lowerXYZBound.x and vertex.x <= boundingBox.upperXYZBound.x) and
                        (vertex.y >= boundingBox.lowerXYZBound.y and vertex.y <= boundingBox.upperXYZBound.y)):
                        self.collisionsList.append((self.segmentList[i], self.segmentList[j]))
                        break
