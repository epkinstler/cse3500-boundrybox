from Point import Point
class LineSegment:
    def __init__(self, p1Point, p2Point):
        self.p1: Point = Point(p1Point)
        self.p2: Point = Point(p2Point)
        self.lowerXYZBound: Point = Point((min(self.p2.x, self.p1.x), min(self.p2.y, self.p1.y), min(self.p2.z, self.p1.z)))
        self.upperXYZBound: Point = Point((max(self.p2.x, self.p1.x), max(self.p2.y, self.p1.y), max(self.p2.z, self.p1.z)))
        self.boundingBoxXYZ: list[Point] = self._getBoundingBoxXYZ()
        self.boundingBoxXY: list[Point] = self._getBoundingBoxXY()

    def _getBoundingBoxXYZ(self):
        vertexList: list[Point] = []
        vertexList.append(Point((self.lowerXYZBound.x, self.lowerXYZBound.y, self.lowerXYZBound.z)))
        vertexList.append(Point((self.lowerXYZBound.x, self.lowerXYZBound.y, self.upperXYZBound.z)))
        vertexList.append(Point((self.lowerXYZBound.x, self.upperXYZBound.y, self.lowerXYZBound.z)))
        vertexList.append(Point((self.lowerXYZBound.x, self.upperXYZBound.y, self.upperXYZBound.z)))
        vertexList.append(Point((self.upperXYZBound.x, self.lowerXYZBound.y, self.lowerXYZBound.z)))
        vertexList.append(Point((self.upperXYZBound.x, self.lowerXYZBound.y, self.upperXYZBound.z)))
        vertexList.append(Point((self.upperXYZBound.x, self.upperXYZBound.y, self.lowerXYZBound.z)))
        vertexList.append(Point((self.upperXYZBound.x, self.upperXYZBound.y, self.upperXYZBound.z)))
        return vertexList

    def _getBoundingBoxXY(self):
        vertexList: list[Point] = []
        vertexList.append(Point((self.lowerXYZBound.x, self.lowerXYZBound.y, 0)))
        vertexList.append(Point((self.lowerXYZBound.x, self.upperXYZBound.y, 0)))
        vertexList.append(Point((self.upperXYZBound.x, self.lowerXYZBound.y, 0)))
        vertexList.append(Point((self.upperXYZBound.x, self.upperXYZBound.y, 0)))
        return vertexList

    def __repr__(self):
        return "<Point x:%s y:%s z:%s> <Point x:%s y:%s z:%s>" % (self.p1.x, self.p1.y, self.p1.z, self.p2.x, self.p2.y, self.p2.z)

    def __str__(self):
        return "(x:%15.6f y:%15.6f z:%20.6f) -- (x:%15.6f y:%15.6f z:%15.6f)" % (self.p1.x, self.p1.y, self.p1.z, self.p2.x, self.p2.y, self.p2.z)

    def getPoint(self, parameter):

        # parametric 3d line equation: tA + (1-t) * B
        # WHERE, 
        # t = parameter in the range 0 <= t <= 1
        # A and B are the start and end points respectively as touples (x, y, z)

        return Point(tuple(map(lambda x, y: x + y, tuple(map(lambda x: x * parameter, self.p1)),
                         tuple(map(lambda x: x * (1 - parameter), self.p2)))))

    def getZfromXY(self, x, y):

        # tAx + (1-t)Bx = x
        # tAx + Bx - tBx = x
        # t(Ax - Bx) = x - Bx
        # t = (x - Bx) / (Ax - Bx)
        # if (Ax - Bx) = 0 problem! use y formula (hopefully)
        # tAy + (1-t)By = y

        if (abs((self.p1.x - self.p2.x)) > 0.000000001):
            return  self.getPoint((x - self.p2.x) / (self.p1.x - self.p2.x))
        elif (abs((self.p1.y - self.p2.y)) > 0.000000001):
            return  self.getPoint((y - self.p2.y) / (self.p1.y - self.p2.y))
        return None


