import math
class Point:
    def __init__(self, tuple):
        self.x = tuple[0]
        self.y = tuple[1]
        self.z = tuple[2]

    def __iter__(self):
        return iter([self.x, self.y, self.z])

    def __repr__(self):
        return "<Point x:%s y:%s z:%s>" % (self.x, self.y, self.z)

    def __str__(self):
        return "(x:%15.6f y:%15.6f z:%15.6f)" % (self.x, self.y, self.z)

    def __eq__(self, other):
        dist_f = lambda p1, p2: math.sqrt(((p2.x-p1.x) ** 2) +
                                          ((p2.y-p1.y) ** 2) +
                                          ((p2.z-p1.z) ** 2))
        if(dist_f(self, other) <= 0.000000001): return True
        else: return False

