from Segments import Segments
import numpy as np
import math

L = Segments()
L.BuildFromFile('input.txt')
outputPath = ('output.txt')

with open(outputPath, mode='a+') as p:
    p.write("3D Intersection Mode\n")
    p.write("|--------------------------------------------------------------------------------------------------------------------Segments-----------------------------------------------------------------------------------------------------------------------| Type |-------------------------Intersect-----------------------|\n")

L.boundingBoxTest()

for i in range(len(L.collisionsList)):
    segmentAB = L.collisionsList[i][0] #parameter t
    segmentCD = L.collisionsList[i][1] #parameter s

    tx = segmentAB.p1.x - segmentAB.p2.x
    sx = segmentCD.p2.x - segmentCD.p1.x
    bx = segmentCD.p2.x - segmentAB.p2.x

    ty = segmentAB.p1.y - segmentAB.p2.y
    sy = segmentCD.p2.y - segmentCD.p1.y
    by = segmentCD.p2.y - segmentAB.p2.y

    tz = segmentAB.p1.z - segmentAB.p2.z
    sz = segmentCD.p2.z - segmentCD.p1.z
    bz = segmentCD.p2.z - segmentAB.p2.z

    # Build matrix A
    A = np.array([[tx, sx],
                  [ty, sy],
                  [tz, sz]])
    # Build matrix B
    B = np.array([bx, by, bz])

    # Solve with least squares approximation index[0] is the solution array 'x'
    # rcond specifies the accuracy to use, -1 uses machine maximum
    M_solution = np.linalg.lstsq(A, B, rcond=-1)

    # unpack the parametric values of t and s
    parameterAB, parameterCD = M_solution[0]

    # calculate the point of closest approach
    pAB = segmentAB.getPoint(parameterAB)
    pCD = segmentCD.getPoint(parameterCD)

    # determine if there is a true intersection
    # note the point class has a custom __eq__ to determine this

    if(pAB == pCD):
        # if it is an intersection, classify it (endpoint to endpoint, midpoint to endpoint, or midpoint to midpoint)
        type_intersection = ""
        is_endpointAB = pAB == segmentAB.p1 or pAB == segmentAB.p2
        is_endpointCD = pCD == segmentCD.p1 or pCD == segmentCD.p2
        if (is_endpointAB and is_endpointCD):
            type_intersection = "e-e"
        elif(is_endpointAB or is_endpointCD):
            type_intersection = "e-m"
        else: type_intersection = "m-m"

        # check for colinearity
        dir_vec_AB = [segmentAB.p2.x - segmentAB.p1.x, segmentAB.p2.y - segmentAB.p1.y, segmentAB.p2.z - segmentAB.p1.z]
        dir_vec_CD = [segmentCD.p2.x - segmentCD.p1.x, segmentCD.p2.y - segmentCD.p1.y, segmentCD.p2.z - segmentCD.p1.z]
        mag_dirAB = np.sqrt(np.dot(dir_vec_AB, dir_vec_AB))
        mag_dirCD = np.sqrt(np.dot(dir_vec_AB, dir_vec_AB))
        angle = np.arccos((np.dot(dir_vec_AB, dir_vec_CD)/(mag_dirAB * mag_dirCD)))
        if (angle < 0.0000001 or math.isnan(angle)): type_intersection = "c"

    else: type_intersection = "none"

    with open(outputPath, mode='a+') as p:
        p.write("| %s\t%s | %4s | %s |\n" % (str(segmentAB), str(segmentCD), type_intersection, pAB))

# I really need to calculate the number of dashes instead of doing them manually but the idea of putting the effort into that after 20+ hours on this project sickens me
with open(outputPath, mode='a+') as p:
    p.write("2D Projection Mode\n")
    p.write("|--------------------------------------------------------------------------------------------------------------------Segments----------------------------------------------------------------------------------------------------------------------| ----Z1---- | ----Z2---- |\n")

L.projectionTest()

for i in range(len(L.collisionsList)):
    segmentAB = L.collisionsList[i][0] 
    segmentCD = L.collisionsList[i][1]

    # get segments in form Ax + By = C
    A_AB = segmentAB.p1.y - segmentAB.p2.y
    B_AB = segmentAB.p2.x - segmentAB.p1.x
    C_AB = (segmentAB.p1.y * segmentAB.p2.x) - (segmentAB.p1.x * segmentAB.p2.y)

    A_CD = segmentCD.p1.y - segmentCD.p2.y
    B_CD = segmentCD.p2.x - segmentCD.p1.x
    C_CD = (segmentCD.p1.y * segmentCD.p2.x) - (segmentCD.p1.x * segmentCD.p2.y)

    # build matrix A
    A = np.array([[A_AB, A_CD],
                  [B_AB, B_CD]])

    # check to see if the lines are parallel using the determinant
    det = np.linalg.det(A)

    # check for intersection
    if (abs(det) >= 0.01): # this method does not have very high accuracy but is easy to implement
        x = (B_CD * C_AB - B_AB * C_CD) / det;
        y = (A_AB * C_CD - A_CD * C_AB) / det;

        # calculate z values
        z1 = segmentAB.getZfromXY(x, y).z
        z2 = segmentCD.getZfromXY(x, y).z

        # return the results
        with open(outputPath, mode='a+') as p:
            p.write("| %s %s  | %10.6f | %10.6f |\n" % (str(segmentAB), str(segmentCD), z1, z2))